<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Task;
use App\Project;

class CreateTask extends Component
{
    public $title = '';
    public $description = '';
    public $status = 1;
    public $imageCount = 1;
    public $statusId = 1;
    public $staffId = 1;
    public $projectId = null;

    public $taskStatuses = [
        ['value' => 1, 'label' => 'Created', 'classes' => 'fas fa-rocket fa-lg', 'colorClass' => 'text-primary', 'styles' => '', 'included' => 'true'],
        ['value' => 3, 'label' => 'Started', 'classes' => 'fas fa-industry fa-lg', 'colorClass' => 'text-secondary', 'styles' => '', 'included' => 'true'],
        ['value' => 0, 'label' => 'Completed', 'classes' => 'fas fa-check-circle fa-lg', 'colorClass' => 'text-success', 'styles' => '', 'included' => 'true']
    ];

    protected function reset()
    {
        $this->title = '';
        $this->description = '';
        $this->imageCount = 1;
        $this->status = 2;
        $this->statusId = 1;
        $this->staffId = 1;
        $this->projectId = null;
    }

    public function back()
    {
        $this->reset();
        $this->redirect('/admin/tasks');
    }

    public function create()
    {
        //TODO: Add proper validation for projectId (is present in projects-table jne.)
        $this->validate([
            'title' => 'required',
            'status' => 'required|integer|min:0|max:6',
            'projectId' => 'required|integer|min:1',
            'imageCount' => 'required|integer|min:1'
        ]);

        Task::create([
            'user_id' => auth()->id(),
            'title' => $this->title,
            'description' => $this->description,
            'image_count' => $this->imageCount,
            'status' => $this->status,
            'staff_id' => auth()->id(),
            'project_id' => $this->projectId
        ]);

        $this->back();
    }

    public function render()
    {
        $projects = Project::all();

        return view('livewire.task.create-task', [
            'projects' => $projects
        ]);
    }
}
