<?php

namespace App\Http\Livewire\Task;

use Livewire\Component;
use App\Task;

class EditTask extends Component
{
    public $id = null;
    public $title = '';
    public $description = '';
    public $imageCount = 1;
    public $status = null;
    public $images = '';

    public $taskStatuses = [
        ['value' => 1, 'label' => 'Created', 'classes' => 'fas fa-rocket fa-lg', 'colorClass' => 'text-primary', 'styles' => '', 'included' => 'true'],
        ['value' => 3, 'label' => 'Started', 'classes' => 'fas fa-industry fa-lg', 'colorClass' => 'text-secondary', 'styles' => '', 'included' => 'true'],
        ['value' => 0, 'label' => 'Completed', 'classes' => 'fas fa-check-circle fa-lg', 'colorClass' => 'text-success', 'styles' => '', 'included' => 'true']
    ];

    protected function reset()
    {
        $this->id = null;
        $this->title = '';
        $this->description = '';
        $this->imageCount = 1;
        $this->status = null;
        $this->images = '';
    }

    public function back()
    {
        $this->reset();
        $this->redirect('/admin/tasks');
    }

    public function mount($id)
    {
        $task = Task::find($id);

        $this->id = $task->id;
        $this->title = $task->title;
        $this->description = $task->description;
        $this->imageCount = $task->image_count;
        $this->status = $task->status;
        $this->images = $task->images;
    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            'imageCount' => 'required|integer|min:1',
            'status' => 'required|integer|min:0|max:6',
        ]);

        $task = Task::find($this->id);

        $staff_id = auth()->id();

        $task->title = $this->title;
        $task->description = $this->description;
        $task->image_count = $this->imageCount;
        $task->staff_id = $staff_id;
        $task->status = $this->status;
        $task->save();

        $this->back();
    }

    public function render()
    {
        return view('livewire.task.edit-task', [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image_count' => $this->imageCount,
            'status' => $this->status,
            'images' => $this->images,
        ]);
    }
}
