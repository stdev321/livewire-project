<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TaskImagesUrl extends Component
{
    public function render()
    {
        return view('livewire.task-images-url');
    }
}
