<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use PHPUnit_Framework_TestCase;
use App\TaskImagesUrl;
use App\Task;

require '../vendor/autoload.php';

class ShutterstockController extends Controller
{
	public function __construct()
    {
        //$this->middleware('auth');
    }
	
    public function index(Request $request) {
		
		/* Authozire request response */ 
		$search = $request->get('term');
		
		 
		$consumerKey = env('CONSUMERKEY');
		$consumerSecret = env('CONSUMERSECRET');
		$access_token = env('STURACCESSTOKEN');
		
		$queryFields = [
		  "query" => $search,
		  "image_type" => "photo",
		  "page" => 1,
		  "per_page" => 5,
		  "sort" => "popular",
		  "view" => "minimal"
		];
		
		$url = "https://api.shutterstock.com/v2/images/search?" . http_build_query($queryFields);
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer $access_token"));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_USERAGENT , "php/curl");
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		$decode_response = json_decode($response);
		//echo '<pre>'; print_r($decode_response); echo '</pre>';
		$images = array();
		foreach($decode_response->data as $key => $value) {
			//echo '<pre>'; print_r($value); echo '</pre>';
			$images[] = $value->assets->preview->url;
		}
		return $images;
		exit;
		
	}

	public function saveTaskImages(Request $request) {

		$validatedData = $request->validate([
            'images' => 'required|string'
        ]);

		$id = $request->get('task_id');
		$images = $request->get('images');
		
        $task = Task::findOrFail($id);
		
		$task->update($validatedData);

		return $task;
    }
}
