<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskImagesUrl extends Model
{
    use SoftDeletes;

    protected $fillable = ['task_id', 'images'];
}
