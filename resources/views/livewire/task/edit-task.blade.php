<div class="container">
    <div class="row justify-content-center">
        <div class="col col-md-8 col-lg-6">
            <div class="card shadow-sm">

                <div class="card-header d-flex justify-content-between align-items-center">
                    <div>
                        Edit task
                    </div>
                    <div>
                        <button 
                            class="btn btn-sm btn-outline-secondary"
                            wire:click="back"
                            title="go back"
                        >
                            <i class="fas fa-arrow-left"></i> Back
                        </button>
                    </div>
                </div>

                <div class="card-body">
				@if (auth()->user()->utype == 'ADM')
                    <div class="form-group ">
                        <label for="editTaskTitle">Title</label>
                        <small class="text-danger">(required)</small>
                        <input 
                            id="editTaskTitle"
                            name="editTaskTitle"
                            type="text"
                            class="form-control form-control {{ $errors->has('title') ? 'border border-danger' : '' }} shadow-sm"
                            placeholder="Title..."
                            aria-describedby="titleErrors"
                            autocomplete="off"
                            wire:model="title"
                        >

                        @if($errors->has('title'))
                            <small id="titleErrors" class="form-text text-danger">{{ $errors->first('title') }}</small>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="editTaskDescription">Description</label>
                        <textarea 
                            id="editTaskDescription"
                            name="editTaskDescription"
                            class="form-control {{ $errors->has('description') ? 'border border-danger' : '' }} shadow-sm"
                            placeholder="Description..."
                            rows="5"
                            value=""
                            wire:model="description"
                            aria-describedby="descriptionErrors"
                            autocomplete="off"
                        ></textarea>

                        @if($errors->has('description'))
                            <small id="descriptionErrors" class="form-text text-danger">{{ $errors->first('description') }}</small>
                        @endif
                    </div>
					
					@endif

                    <input type="hidden" id="task_id" wire:model="id" />

                    <div class="form-group">
                        <label for="editTaskImagesCount">Number of Images</label>
                        <input 
                            id="editTaskImagesCount"
                            name="editTaskImagesCount"
                            type="text"
                            class="form-control {{ $errors->has('imageCount') ? 'border border-danger' : '' }}  shadow-sm"
                            placeholder="Number of Images..."
                            aria-describedby="imageCountErrors"
                            wire:model="imageCount"
                            autocomplete="off"
                            autofocus
                            @if (auth()->user()->utype == 'USR') disabled @endif
                        >

                        @if($errors->has('imageCount'))
                            <small id="imageCountErrors" class="form-text text-danger">{{ $errors->first('imageCount') }}</small>
                        @endif
                    </div>

					<input class="form-control" type="hidden" wire:model="staff_id" name="staff_id" value="{{ auth()->user()->id }}">
                    <div class="form-group">
                        <label for="editTaskStatus">Status</label>
                        <select 
                            id="editTaskStatus" 
                            name="editTaskStatus"  
                            class="form-control form-control shadow-sm"
                            wire:model="status" 
                        >
                            @foreach($taskStatuses as $taskStatus)
                                <option 
                                    name="{{ $taskStatus['label'] }}" 
                                    value="{{ $taskStatus['value'] }}" 
                                    {{ $status == $taskStatus['value'] ? 'selected' : '' }}
                                >
                                    {{ $taskStatus['label'] }}
                                </option>
                            @endforeach
                        </select>

                        @if($errors->has('status'))
                            <small id="statusErrors" class="form-text text-danger">{{ $errors->first('status') }}</small>
                        @endif
                    </div>
					
					@if (auth()->user()->utype == 'USR')
						<div class="form-group">
							<label for="editTaskImages">Search Image</label>
							<div class="input-group">
								<input id="search" name="search" type="text" class="form-control" placeholder="Search" />
							</div>
                            <div class="input-group">
                            <div id="append_images"></div>
                            <div id="selected_images" wire:model="images"></div>
                            </div>
						</div>

                    <div class="form-group">
                        <button 
                            class="btn btn-success form-control" 
                            id="update_btn"
                            type="submit"
                            title="Upload Images"
                        >
                        Upload Images
                        </button>
                    </div>
                    @endif
                    <div class="form-group">
                        <button 
                            class="btn btn-success form-control" 
                            wire:click="update" 
                            type="submit"
                            title="Save task"
                        >
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
 jQuery(document).ready(function($) {
  $(document).on("click",".select_image",function() {
    var images_array = $('.selected_images_hidden').length;
    var upload_images_count = $('#editTaskImagesCount').val();
    console.log(images_array, upload_images_count);
    if(parseInt(images_array) < parseInt(upload_images_count)) {
       
        $('#selected_images').append("<div class='image_"+ $(this).data("id") +"'><button type='submit' class='close'><span>&times;</span></button><img src='"+ $(this).data("id") +"' /><input type='hidden' class='selected_images_hidden' value='"+ $(this).data("id") +"'/></div>");
    } else {
        alert('Uploading limit has been Exceeded!');
    }
    
  });

  $(document).on("click",".close",function() {
        $(this).parent().remove();
  });

  $(document).on("click", "#update_btn", function(){
      task_id = $('#task_id').val();
      console.log(task_id);
      var img_arr = [];
      var img_;
        $('.selected_images_hidden').each(function(index) {
             img_= $(this).val();
             img_arr.push(img_);
        }),
        
        img_arr = JSON.stringify(img_arr);
        console.log(img_arr);
        $.ajax({
            url: "{{url('saveimages')}}",
            type:"POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                task_id : task_id,
                images : img_arr
            },
            success: function(data){
                alert('Images has been uploaded successfully!');
            },
            error: function (jqXHR, exception) {
                alert('Something went wrong, Please try again later!');
            }
        });
        
  });

    $("#search").autocomplete({
 
        source: function(request, response) {
            $.ajax({
            url: "{{url('shutterstock')}}",
            data: {
                    term : request.term
             },
            dataType: "json",
            success: function(data){
               var html_data = "";

                for (let i = 0; i < data.length; i++) {
                    html_data = html_data+"<img src='" + data[i] + "' /><input class='select_image' type='checkbox' data-id='"+ data[i] +"' />";
                }
                $('#append_images').html(html_data);
            }
        });
    },
    minLength: 1
 });
});
 
</script>   
